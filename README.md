# E-Commerce

##Overview
This Application consists of four tabs (customer,order,product,orderlistitems) this tabs helps to navigates through application.
1. Customer tab helps to see all the data of customer and helps to edit it.
2. Order tab helps to see all the data of order and helps to edit it.
3. Product tab helps to see all the data of product and helps to edit it.
4. orderlistitem tab helps to see all the data of orderlisitem and helps to edit it.

###Team
1. Order - Rayaan hmed
2. Order Line - Manikantha
3. Customer - Rayaan Ahmed
4. Product - Aakash

###Bit Bucket Link
https://bitbucket.org/webapps01/ecommerce-web-page/src/master/

###Heroku Link
https://ecommerce-a04.herokuapp.com/